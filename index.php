<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    
        <link href="https://fonts.googleapis.com/css?family=Poppins:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Exo+2:300,300i,400,400i,700,700i&amp;subset=cyrillic" rel="stylesheet">
        <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
        <link rel="stylesheet" href="js/vendor/swiper/css/swiper.min.css">
        <link rel="stylesheet" href="css/main.css">

    </head>
    <body>
    
        <header class="header">
            <div class="container">
                <ul class="header_top">
                    <li>
                        <div class="header_logo">
                            <img src="img/logo.png" class="img-fluid" alt="">
                        </div>
                    </li>
                    <li>
                        <a href="#auth" class="btn btn_white btn_modal">Sign up</a>
                    </li>
                    <li>
                        <ul class="header_nav">
                            <li><a href="#work">How It Works</a></li>
                            <li><a href="#why">Advantages</a></li>
                        </ul>
                    </li>
                </ul>
                <h1>
                    MEET the <strong>new</strong><br/>
                    SUPER-PROFITABLE<br/>
                    TRADING
                </h1>
                <div class="header_slogan">
                    Make up to <strong>700%</strong><br/>
                    in profit on the news
                </div>
                <div class="header_tab">
                    <div class="header_tab_wrap">
                        <img src="img/header_tablet.png" class="img-fluid" alt="">
                    </div>
                </div>
                <a href="#vip" class="btn btn_jewel btn_modal">
                    <i>
                        <svg class="ico-svg"  viewBox="0 0 31 29" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="img/sprite_icons.svg#icon_jewel" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </i>
                    <span>GET VIP ACCESS</span>
                </a>
                <div class="header_text">
                    <p>* Bonus for the first 1,000 clients</p>
                    <p>Every day in the world there are <strong>1,000 events that affect the market</strong>. Imagine you could easily earn on the price of assets by knowing what's happening in the world. It doesn't matter where the price goes. What matters is that you know, you act, and you earn. <strong>Now</strong>.</p>
                </div>
            </div>
            <div class="header_slider">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        
                        <div class="swiper-slide">
                            <div class="user">
                                <div class="user_photo">
                                    <img src="images/user_01.png" class="img-fluid" alt="">
                                </div>
                                <div class="user_name">
                                    <span>Peter T.</span>
                                    <i class="flag flag_de"></i>
                                </div>
                                <div class="user_value">Just earned €205</div>
                            </div>
                        </div>
                        
                        <div class="swiper-slide">
                            <div class="user">
                                <div class="user_photo">
                                    <img src="images/user_02.png" class="img-fluid" alt="">
                                </div>
                                <div class="user_name">
                                    <span>Helen E.</span>
                                    <i class="flag flag_pt"></i>
                                </div>
                                <div class="user_value">Just earned €1 304</div>
                            </div>
                        </div>
                        
                        <div class="swiper-slide">
                            <div class="user">
                                <div class="user_photo">
                                    <img src="images/user_03.png" class="img-fluid" alt="">
                                </div>
                                <div class="user_name">
                                    <span>Nikilas S.</span>
                                    <i class="flag flag_sp"></i>
                                </div>
                                <div class="user_value">Just earned €4 008</div>
                            </div>
                        </div>
                        
                        <div class="swiper-slide">
                            <div class="user">
                                <div class="user_photo">
                                    <img src="images/user_04.png" class="img-fluid" alt="">
                                </div>
                                <div class="user_name">
                                    <span>Knud J.</span>
                                    <i class="flag flag_de"></i>
                                </div>
                                <div class="user_value">Just earned €390</div>
                            </div>
                        </div>
                        
                        <div class="swiper-slide">
                            <div class="user">
                                <div class="user_photo">
                                    <img src="images/user_05.png" class="img-fluid" alt="">
                                </div>
                                <div class="user_name">
                                    <span>Martina T.</span>
                                    <i class="flag flag_ch"></i>
                                </div>
                                <div class="user_value">Just earned €2 390</div>
                            </div>
                        </div>
                        
                        <div class="swiper-slide">
                            <div class="user">
                                <div class="user_photo">
                                    <img src="images/user_06.png" class="img-fluid" alt="">
                                </div>
                                <div class="user_name">
                                    <span>George M.</span>
                                    <i class="flag flag_gb"></i>
                                </div>
                                <div class="user_value">Just earned €2 250</div>
                            </div>
                        </div>
    
                        <div class="swiper-slide">
                            <div class="user">
                                <div class="user_photo">
                                    <img src="images/user_07.png" class="img-fluid" alt="">
                                </div>
                                <div class="user_name">
                                    <span>Marco I.</span>
                                    <i class="flag flag_it"></i>
                                </div>
                                <div class="user_value">Just earned €8 140</div>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </header>

        <section class="work" id="work">
            <div class="container">
                <h2>How does it work?</h2>
            
                <div class="work_row">
                    <div class="work_graph">
                    
                        <ul class="graph_legend">
                            <li>
                                <span>Investment</span>
                                <strong>€ 100</strong>
                            </li>
                            <li>
                                <span>Income per sec</span>
                                <strong>€ 10</strong>
                            </li>
                            <li>
                                <span>Total Income</span>
                                <strong>€ 120</strong>
                            </li>
                        </ul>
                    
                        <div class="graph_mobile">
                            <img src="img/graph_mobile.png" class="img-fluid" alt="">
                        </div>
                    
                        <div class="graph_desktop">
                            <img src="img/notebook.png" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="work_content">
                        <ol class="work_step">
                            <li>
                                <strong>INVEST</strong>
                                <span>And lock in the price corridor</span>
                            </li>
                            <li>
                                <strong>EARN</strong>
                                <span>Every second while the price is inside the corridor</span>
                            </li>
                            <li>
                                <strong>GET PAID</strong>
                                <span>As soon as the price reaches the boundary of the corridor</span>
                            </li>
                        </ol>
                    </div>
                </div>
        
            </div>
        </section>
    
        <section class="why" id="why">
            <div class="container">
                <h2>Why should you try EDS Trading?</h2>
                <ul class="why_list">
                    <li>
                        You no longer need to guess<br/>
                        the direction of price<br/>
                        movement of an asset
                    </li>
                    <li>
                        You can trade both on<br/>
                        calm and volatile markets
                    </li>
                    <li>
                        High payouts<br/>
                        of up to 700%
                    </li>
                    <li>
                        There is no<br/>
                        expiration time
                    </li>
                    <li>
                        Minimal<br/>
                        risks
                    </li>
                </ul>
                <div class="text_center">
                    <a href="#auth" class="btn btn_jewel btn_modal">
                        <i>
                            <svg class="ico-svg"  viewBox="0 0 31 29" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon_jewel" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <span>SIGN UP</span>
                    </a>
                </div>
            </div>
        </section>
    
        <footer class="footer">
            <div class="container">© 2018 EDS TRADING</div>
        </footer>

        <div class="privacy">
            <div class="container">
                <h4>Cookies Policy</h4>
                <p>We and our partners use technology such as cookies on our site to personalise content and ads, provide social media features, and analyse our traffic. Click below to consent to the use of this technology across the web. You can change your mind and change your consent choices at anytime by changing preferences in the browser settings.</p>
                <a href="#" class="btn btn_sm btn_accept">Accept</a>
            </div>
        </div>

        <div class="hide">
            <div class="modal" id="auth">
                <div class="modal_title">Registration</div>
                <form class="form" name="form" method="POST" action="javascript:void(0);">
                    <input type="hidden" value="form" name="form reg">
                    <div class="form_group valid email required">
                        <input class="form_control" type="text" name="email" placeholder="">
                        <div class="form_placeholder"><sup>*</sup>Email</div>
                    </div>
                    <div class="text_center">
                        <button type="submit" class="btn btn_sm btn_send">Registration</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="hide">
            <div class="modal" id="vip">
                <div class="modal_title">Registration</div>
                <form class="form" name="form" method="POST" action="javascript:void(0);">
                    <input type="hidden" value="form" name="form vip">
                    <div class="form_group valid email required">
                        <input class="form_control" type="text" name="email" placeholder="">
                        <div class="form_placeholder"><sup>*</sup>Email</div>
                    </div>
                    <div class="text_center">
                        <button type="submit" class="btn btn_sm btn_send">Registration</button>
                    </div>
                </form>
            </div>
        </div>



        <!-- Окно Спасибо -->
        <div class="hide">
            <a href="#thanks" class="btn_thanks btn_modal"></a>
            <div class="modal" id="thanks">
                <div class="modal_title">Thank you</div>
                <div class="modal_thanks">Your application is pending</div>
                <br/>
                <div class="text_center">
                    <button data-fancybox-close class="btn  btn_sm btn_close">Close</button>
                </div>
            </div>
        </div>
        <!-- -->


        <script src="js/vendor/modernizr-3.5.0.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
        <script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
        <script src="js/vendor/swiper/js/swiper.min.js"></script>
        <script src="js/vendor/svg4everybody.legacy.min.js"></script>
        <script src="js/vendor/jquery.cookie.js"></script>
        <script src="js/vendor/validator.js"></script>
        <script src="js/main.js"></script>
    

    </body>
</html>
